json.array!(@admin_sponsors) do |admin_sponsor|
  json.extract! admin_sponsor, :id, :name, :address, :phone, :email, :instagram, :facebook
  json.url admin_sponsor_url(admin_sponsor, format: :json)
end
