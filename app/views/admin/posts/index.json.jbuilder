json.array!(@admin_posts) do |admin_post|
  json.extract! admin_post, :id, :title, :highlight, :content, :publish_date, :status, :enable_comments
  json.url admin_post_url(admin_post, format: :json)
end
