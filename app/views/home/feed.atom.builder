atom_feed do |feed|
  feed.title "Lalu Lins"
  feed.updated @posts.maximum(:updated_at)

  @posts.each do |post|
    feed.entry post, published: post.publish_date do |entry|
      entry.title post.title
      entry.content "type" => "html" do
        if post.main_image.exists?
          entry.cdata! "<div style='text-align: center'>#{image_tag post.main_image.url(:big), alt: post.main_image_subtitle || post.title}</div>"
        end
        entry.cdata!(post.content)
        post.photos.each do |photo|
          if photo.image.exists?
            entry.cdata! "<div style='text-align: center'>#{image_tag(photo.image.url(:big), alt: photo.subtitle || post.title)}</div>"
            entry.cdata! "<div style='text-align: center'>#{photo.subtitle}</div><br /><br />"
          end
        end

        unless post.clothes_credits.blank?
          entry.cdata! "<div style='text-align: center'>Créditos: #{post.clothes_credits}</div>"
        end
        unless post.photo_credits.blank?
          entry.cdata! "<div style='text-align: center'>Photography: #{post.photo_credits}</div><br /><br />"
        end

        unless post.sponsor.nil?
          entry.cdata! "<div>#{image_tag post.sponsor.logo.url}</div>"
          entry.cdata! "<div>#{post.sponsor.name}</div>"
          unless post.sponsor.site.blank?
            entry.cdata! "<div>#{link_to (image_tag 'icon-site.png', class: 'icon icon-site'), post.sponsor.site, target: '_blank'} #{link_to post.sponsor.site.gsub(/^(http:\/\/)/, ''), post.sponsor.site, target: '_blank'}</div>"
          end
          unless post.sponsor.facebook.blank?
            entry.cdata! "<div>#{link_to (image_tag 'icon-facebook.png', class: 'icon icon-facebook'), post.sponsor.facebook.gsub(/(http:\/\/|https:\/\/)*(www.)*(facebook.com|fb.com)(\/)([^\?]+)(\?)?(.+)*/, 'http://fb.com/\5'), target: '_blank'} #{link_to post.sponsor.facebook.gsub(/(http:\/\/|https:\/\/)*(www.)*(facebook.com|fb.com)(\/)([^\?]+)(\?)?(.+)*/, 'fb.com/\5'), post.sponsor.facebook.gsub(/(http:\/\/|https:\/\/)*(www.)*(facebook.com|fb.com)(\/)([^\?]+)(\?)?(.+)*/, 'http://fb.com/\5'), target: '_blank'}</div>"
          end
          unless post.sponsor.instagram.blank?
            entry.cdata! "<div>#{link_to (image_tag 'icon-instagram.png', class: 'icon icon-instagram'), post.sponsor.instagram.gsub(/(http:\/\/)*(www.)*(instagram.com)*(\/)*(\@)*(.+)/, 'http://instagram.com/\6'), target: '_blank'} #{link_to post.sponsor.instagram.gsub(/(^(http:\/\/)(www.instagram.com|instagram.com)(\/))|^@/, ''), post.sponsor.instagram.gsub(/(http:\/\/)*(www.)*(instagram.com)*(\/)*(\@)*(.+)/, 'http://instagram.com/\6'), target: '_blank'}</div>"
          end
          unless post.sponsor.whatsapp.blank?
            entry.cdata! "<div>#{image_tag 'icon-whatsapp2.png', class: 'icon icon-whatsapp'} #{post.sponsor.whatsapp}</div>"
          end
        end
        entry.cdata! "<hr />"
      end
      # entry.content post.content
      entry.author do |author|
        author.name "Lalu Lins"
      end
    end
  end
end