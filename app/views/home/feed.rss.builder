xml.instruct! :xml, version: "1.0"
xml.rss version: "2.0" do
  xml.channel do
    xml.title "Lalu Lins"
    xml.description "Um blog para pensar e discutir Moda sem regras e sem receitas, mas com muita paixão. Um espaço de trocas para falar de Moda como uma forma de expressão pessoal."
    xml.link 'http://www.lalulins.com.br'

    @posts.each do |post|
      xml.item do
        xml.title post.title
        # xml.description post.content
        xml.description do
          if post.main_image.exists?
            xml.cdata! "<div style='text-align: center'>#{image_tag post.main_image.url(:big), alt: post.main_image_subtitle || post.title}</div>"
          end
          xml.cdata! post.content
          post.photos.each do |photo|
            if photo.image.exists?
              xml.cdata! "<div style='text-align: center'>#{image_tag(photo.image.url(:big), alt: photo.subtitle || post.title)}</div>"
              xml.cdata! "<div style='text-align: center'>#{photo.subtitle}</div><br /><br />"
            end
          end

          unless post.clothes_credits.blank?
            xml.cdata! "<div style='text-align: center'>Créditos: #{post.clothes_credits}</div>"
          end
          unless post.photo_credits.blank?
            xml.cdata! "<div style='text-align: center'>Photography: #{post.photo_credits}</div><br /><br />"
          end

          unless post.sponsor.nil?
            xml.cdata! "<div>#{image_tag post.sponsor.logo.url}</div>"
            xml.cdata! "<div>#{post.sponsor.name}</div>"
            unless post.sponsor.site.blank?
              xml.cdata! "<div>#{link_to (image_tag 'icon-site.png', class: 'icon icon-site'), post.sponsor.site, target: '_blank'} #{link_to post.sponsor.site.gsub(/^(http:\/\/)/, ''), post.sponsor.site, target: '_blank'}</div>"
            end
            unless post.sponsor.facebook.blank?
              xml.cdata! "<div>#{link_to (image_tag 'icon-facebook.png', class: 'icon icon-facebook'), post.sponsor.facebook.gsub(/(http:\/\/|https:\/\/)*(www.)*(facebook.com|fb.com)(\/)([^\?]+)(\?)?(.+)*/, 'http://fb.com/\5'), target: '_blank'} #{link_to post.sponsor.facebook.gsub(/(http:\/\/|https:\/\/)*(www.)*(facebook.com|fb.com)(\/)([^\?]+)(\?)?(.+)*/, 'fb.com/\5'), post.sponsor.facebook.gsub(/(http:\/\/|https:\/\/)*(www.)*(facebook.com|fb.com)(\/)([^\?]+)(\?)?(.+)*/, 'http://fb.com/\5'), target: '_blank'}</div>"
            end
            unless post.sponsor.instagram.blank?
              xml.cdata! "<div>#{link_to (image_tag 'icon-instagram.png', class: 'icon icon-instagram'), post.sponsor.instagram.gsub(/(http:\/\/)*(www.)*(instagram.com)*(\/)*(\@)*(.+)/, 'http://instagram.com/\6'), target: '_blank'} #{link_to post.sponsor.instagram.gsub(/(^(http:\/\/)(www.instagram.com|instagram.com)(\/))|^@/, ''), post.sponsor.instagram.gsub(/(http:\/\/)*(www.)*(instagram.com)*(\/)*(\@)*(.+)/, 'http://instagram.com/\6'), target: '_blank'}</div>"
            end
            unless post.sponsor.whatsapp.blank?
              xml.cdata! "<div>#{image_tag 'icon-whatsapp2.png', class: 'icon icon-whatsapp'} #{post.sponsor.whatsapp}</div>"
            end
          end

          xml.cdata! "<hr />"
        end
        xml.pubDate post.publish_date.to_s(:rfc822)
        xml.link post_url(post)
        xml.guid post_url(post)
      end
    end
  end
end