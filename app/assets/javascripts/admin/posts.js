(function () {
    var admin_posts_ready, posts_tags, posts_crop_main_image, posts_fill_photo_order, posts_sort_images, posts_show_coords;

    admin_posts_ready = function () {
        $('form').on('click', '.remove_fields', function (event) {
            $(this).prev('input[type=hidden]').val('1');
            $(this).closest('fieldset').hide();
            event.preventDefault();
        });
        $('form').on('click', '.add_fields', function (event) {
            var regexp, time;
            time = new Date().getTime();
            regexp = new RegExp($(this).data('id'), 'g');
//            $(this).before($(this).data('fields').replace(regexp, time));
            $(this).prev().append($(this).data('fields').replace(regexp, time));
            posts_fill_photo_order();
            event.preventDefault();
        });

        posts_tags();
        posts_crop_main_image();
        posts_fill_photo_order();
        posts_sort_images();
    };
    $(document).ready(admin_posts_ready);
    $(document).on('page:load', admin_posts_ready);

    posts_tags = function () {
        $("#post_tags").tagit({
            fieldName: "tag_list[]",
            allowSpaces: true
        });
    };

    posts_crop_main_image = function () {
        var x1 = $('#admin_post_main_image_crop_x1').val() / 3;
        var y1 = $('#admin_post_main_image_crop_y1').val() / 3;
        var x2 = $('#admin_post_main_image_crop_x2').val() / 3;
        var y2 = $('#admin_post_main_image_crop_y2').val() / 3;
        $('#main_image').Jcrop({
            onChange: posts_show_coords,
            onSelect: posts_show_coords,
            setSelect: [x1, y1, x2, y2],
            aspectRatio: 452 / 302,
            keySupport: false
        });
    };

    posts_show_coords = function (c) {
        $('#admin_post_main_image_crop_x1').val(c.x);
        $('#admin_post_main_image_crop_y1').val(c.y);
        $('#admin_post_main_image_crop_x2').val(c.x2);
        $('#admin_post_main_image_crop_y2').val(c.y2);
        $('#crop_width').val(c.w);
        $('#crop_height').val(c.h);
    };

    posts_fill_photo_order = function () {
        $('.photo_order').each(function (index, obj) {
            obj.value = index + 1;
        });
    };

    posts_sort_images = function () {
        $("#sortable").sortable({
            placeholder: "ui-state-highlight",
            stop: function (event, ui) {
                posts_fill_photo_order();
            }
        });
    };

}).call(this);