/**
 * Created by nielsonrolim on 8/9/14.
 */

(function () {
    var posts_ready;

    posts_ready = function () {

        if (typeof category_id !== 'undefined') {
            $("#menu_categoria_" + category_id).addClass('active_menu_item');
            $("#rodape_categoria" + category_id).addClass('active_menu_item');
        }

    };
    $(document).ready(posts_ready);
    $(document).on('page:load', posts_ready);


}).call(this);