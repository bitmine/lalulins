// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives. // = require turbolinks
//
//= require jquery
//= require jquery_ujs
//= require jquery.ui.all
//= require jquery.Jcrop
//= require twitter/bootstrap
//= require ckeditor/override
//= require ckeditor/init
//= require_tree .

(function () {
    var application_ready;

    application_ready = function () {

        jQuery.fn.extend({
            scrollToMe: function () {
                var x = jQuery(this).offset().top - 100;
                jQuery('html,body').animate({scrollTop: x}, 400);
            }});

        jQuery(function () {
            $("#busca_input").focus(function () {
                if ($("#busca_input").val() == 'Buscar') {
                    $("#busca_input").val('');

                }
            });

            $("#busca_input").focusout(function () {
                if (!$("#busca_input").val()) {
                    $("#busca_input").val('Buscar');

                }
            });
        });

        jQuery('#allinone_carousel_charming').allinone_carousel({
            skin: 'charming',
            width: 990,
            autoPlay: 3,
            resizeImages: true,
            autoHideBottomNav: false,
            showElementTitle: false,
            verticalAdjustment: 50,
            showPreviewThumbs: false,
            numberOfVisibleItems: 5,
            nextPrevMarginTop: 23,
            playMovieMarginTop: 0,
            bottomNavMarginBottom: -10,
            responsive: true,
            showBottomNav: false,
            showCircleTimer: false,
            showNavArrows: false,
            enableTouchScreen: true
        });

        $('#wrapper img').mouseenter(function(){
            setTimeout(function(){
                $("body").find("[title='Pin it!']").addClass('pointer');
            },10)
        })

        $('.allinone_carousel img').attr('nopin','nopin');

        $.scrollUp({
            scrollName: 'scrollUp', // Element ID
            scrollDistance: 300, // Distance from top/bottom before showing element (px)
            scrollFrom: 'top', // 'top' or 'bottom'
            scrollSpeed: 300, // Speed back to top (ms)
            easingType: 'linear', // Scroll to top easing (see http://easings.net/)
            animation: 'fade', // Fade, slide, none
            animationInSpeed: 200, // Animation in speed (ms)
            animationOutSpeed: 200, // Animation out speed (ms)
            scrollText: '<i class="icon-up-open"></i>', // Text for element, can contain HTML
            scrollTitle: 'Voltar para o topo', // Set a custom <a> title if required. Defaults to scrollText
            scrollImg: false, // Set true to use image
            activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
            zIndex: 1001 // Z-Index for the overlay
        });
    };

    $(document).ready(application_ready);
    $(document).on('page:load', application_ready);

}).call(this);
