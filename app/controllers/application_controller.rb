class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :hot_posts

  private
  def hot_posts
    @hot_posts = Post.where("status = ? and highlight and main_image_file_name is not null", 'Publicado').order('publish_date desc').limit(6)
  end
end
