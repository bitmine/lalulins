class Admin::PostsController < ApplicationController
  require 'RMagick'
  include Magick

  layout "admin"
  before_filter :authenticate_user!
  before_action :set_admin_post, only: [:show, :edit, :update, :destroy]


  # GET /admin/posts
  # GET /admin/posts.json
  def index
    @admin_posts = Admin::Post.all
    if params[:search]
      @admin_posts = @admin_posts.text_search(params[:search])
    end
    @admin_posts = @admin_posts.order('publish_date desc')
    @admin_posts = @admin_posts.page(params[:page]).per_page(30)
    return @admin_posts
  end

  # GET /admin/posts/1
  # GET /admin/posts/1.json
  def show
  end

  # GET /admin/posts/new
  def new
    @admin_post = Admin::Post.new
    @admin_post.enable_comments = true
    @admin_post.soundcloud_show_artwork = true
  end

  # GET /admin/posts/1/edit
  def edit
  end

  # POST /admin/posts
  # POST /admin/posts.json
  def create
    @admin_post = Admin::Post.new(admin_post_params)

    tag_list = params['tag_list']
    @admin_post.tag_list = tag_list.join(', ') if tag_list

    respond_to do |format|
      if @admin_post.save
        multiple_images = params[:multiple_images]
        unless multiple_images.nil?
          multiple_images.each do |file_data|
            if file_data.respond_to?(:path)
              Photo.create!(image: file_data.tempfile, post_id: @admin_post.id)
            end
          end
        end

        format.html { redirect_to edit_admin_post_path(@admin_post), notice: 'Post foi adicionado com sucesso.' }
        format.json { render action: 'show', status: :created, location: @admin_post }
      else
        format.html { render action: 'new' }
        format.json { render json: @admin_post.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/posts/1
  # PATCH/PUT /admin/posts/1.json
  def update

    tag_list = params['tag_list']
    @admin_post.tag_list = tag_list.join(', ') if tag_list

    if @admin_post.main_image.exists?
      params[:admin_post][:main_image_crop_x1] = params[:admin_post][:main_image_crop_x1].to_i * 3
      params[:admin_post][:main_image_crop_y1] = params[:admin_post][:main_image_crop_y1].to_i * 3
      params[:admin_post][:main_image_crop_x2] = params[:admin_post][:main_image_crop_x2].to_i * 3
      params[:admin_post][:main_image_crop_y2] = params[:admin_post][:main_image_crop_y2].to_i * 3
      x = params[:admin_post][:main_image_crop_x1]
      y = params[:admin_post][:main_image_crop_y1]
      w = params[:crop_width].to_i * 3
      h = params[:crop_height].to_i * 3

      @admin_post.save_cropped_images(x, y, w, h)
    end

    delete_main_image = params['delete_main_image']
    if delete_main_image
      @admin_post.main_image = nil
      @admin_post.main_image_subtitle = nil
      params[:admin_post][:main_image] = nil
      params[:admin_post][:main_image_subtitle] = nil

      @admin_post.delete_cropped_images

      params[:admin_post][:main_image_crop_x1] = nil
      params[:admin_post][:main_image_crop_y1] = nil
      params[:admin_post][:main_image_crop_x2] = nil
      params[:admin_post][:main_image_crop_y2] = nil
    end

    if params[:admin_post][:soundcloud].blank?
      params[:admin_post][:soundcloud_trackid] = nil
    end

    if params[:admin_post][:youtube_url].blank?
      params[:admin_post][:youtube_videoid] = nil
    end

    respond_to do |format|
      if @admin_post.update(admin_post_params)
        multiple_images = params[:multiple_images]
        unless multiple_images.nil?
          multiple_images.each do |file_data|
            if file_data.respond_to?(:path)
              Photo.create!(image: file_data.tempfile, post_id: @admin_post.id)
            end
          end
        end

        format.html { redirect_to edit_admin_post_path(@admin_post), notice: 'Post foi atualizado com sucesso.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @admin_post.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/posts/1
  # DELETE /admin/posts/1.json
  def destroy
    @admin_post.destroy
    respond_to do |format|
      format.html { redirect_to admin_posts_url }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_admin_post
    @admin_post = Admin::Post.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def admin_post_params
    params.require(:admin_post).permit(:title, :highlight, :content, :acknowledgment, :publish_date, :status, :enable_comments, :category_id, :main_image, :main_image_subtitle, :tag_list, :main_image_crop_x1, :main_image_crop_y1, :main_image_crop_x2, :main_image_crop_y2, :photo_credits, :clothes_credits, :sponsor_id, :soundcloud, :soundcloud_show_artwork, :soundcloud_trackid, :youtube_url, :youtube_videoid, :spotify_uri, :spotify_embed, photos_attributes: [:id, :subtitle, :credits, :photo_order, :post_id, :_destroy, :image])
  end
end
