class Admin::SponsorsController < ApplicationController
  layout "admin"
  before_filter :authenticate_user!
  before_action :set_admin_sponsor, only: [:show, :edit, :update, :destroy]

  # GET /admin/sponsors
  # GET /admin/sponsors.json
  def index
    @admin_sponsors = Admin::Sponsor.all
  end

  # GET /admin/sponsors/1
  # GET /admin/sponsors/1.json
  def show
  end

  # GET /admin/sponsors/new
  def new
    @admin_sponsor = Admin::Sponsor.new
  end

  # GET /admin/sponsors/1/edit
  def edit
  end

  # POST /admin/sponsors
  # POST /admin/sponsors.json
  def create
    @admin_sponsor = Admin::Sponsor.new(admin_sponsor_params)

    respond_to do |format|
      if @admin_sponsor.save
        format.html { redirect_to admin_sponsors_path, notice: 'Sponsor was successfully created.' }
        format.json { render action: 'show', status: :created, location: @admin_sponsor }
      else
        format.html { render action: 'new' }
        format.json { render json: @admin_sponsor.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /admin/sponsors/1
  # PATCH/PUT /admin/sponsors/1.json
  def update
    respond_to do |format|
      if @admin_sponsor.update(admin_sponsor_params)
        format.html { redirect_to admin_sponsors_path, notice: 'Sponsor was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @admin_sponsor.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /admin/sponsors/1
  # DELETE /admin/sponsors/1.json
  def destroy
    @admin_sponsor.destroy
    respond_to do |format|
      format.html { redirect_to admin_sponsors_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_admin_sponsor
      @admin_sponsor = Admin::Sponsor.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def admin_sponsor_params
      params.require(:admin_sponsor).permit(:name, :address, :phone, :email, :site, :instagram, :facebook, :whatsapp, :logo)
    end
end
