class HomeController < ApplicationController
  def index
    @hot_posts = Post.where("status = ? and highlight and main_image_file_name is not null", 'Publicado').order('publish_date desc').limit(6)
    @last_post = Post.where("status = ?", 'Publicado').order('publish_date desc').first
  end

  def contato
    @contact_form = ContactForm.new
  end

  def feed
    @posts = Post.where(status: 'Publicado').order('publish_date desc')
  end

  def send_contato
    @contact_form = ContactForm.new(params[:contact_form])

    if @contact_form.deliver
      flash[:notice] = "Sua mensagem foi enviada com sucesso. Obrigado pelo contato!"
      redirect_to contato_path
    else
      render :contato
    end
  end

  def anuncie
    @anuncie_form = AdvertiseForm.new
  end

  def send_anuncie
    @anuncie_form = AdvertiseForm.new(params[:advertise_form])

    if @anuncie_form.deliver
      flash[:notice] = "Formulário enviado com sucesso. Obrigado pelo contato!"
      redirect_to anuncie_path
    else
      render :anuncie
    end
  end

  def sobre

  end

end
