class PostsController < ApplicationController

  before_filter :authenticate_admin_user, :only => [:preview]


  def post
    if params[:category_id]
      @category = Category.find params[:category_id]
    else
      @category = nil
    end
    begin
      @post = Post.where('status = ?', 'Publicado').find(params[:id])
    rescue ActiveRecord::RecordNotFound => e
      redirect_to root_path
    end

  end

  def busca
    query = params['search']
    @posts = Post.where('status = ?', 'Publicado').text_search(query).order('publish_date desc').page(params[:page]).per_page(15)
  end

  def tags
    @tag = params['tag']
    @posts = Post.where('status = ?', 'Publicado').order('publish_date desc').tagged_with(@tag).order('publish_date desc').page(params[:page]).per_page(15)
  end

  def categoria
    category = params['category_id']
    @category = Category.find(category)
    @posts = Post.where('category_id = ? and status = ?', category, 'Publicado').order('publish_date desc').page(params[:page]).per_page(15)
  end

  def preview
    if params[:category_id]
      @category = Category.find params[:category_id]
    else
      @category = nil
    end
    @post = Post.find params[:id]
  end

  private

  def authenticate_admin_user
    if user_signed_in?
      return true
    else
      redirect_to user_root_path
    end
  end
end
