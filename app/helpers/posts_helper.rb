module PostsHelper
  def link_to_post_with_category(label, post_id, category_id=nil )
    link_to label, {controller: :posts, action: :post, id: post_id, category_id: category_id}
  end
end
