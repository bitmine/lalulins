class Photo < ActiveRecord::Base
  default_scope { order('photo_order asc') }
  belongs_to :post

  has_attached_file :image,
                    :styles => {original: "1200x3750>", big: "700x2187.5>", thumb: "150x113#"},
                    :convert_options => {original: "-quality 90", big: "-quality 90"},
                    :default_url => "sem-foto.png",
                    :url => "/images/photos/:attachment/:id_partition/:style/:filename",
                    :path => ":rails_root/public/images/photos/:attachment/:id_partition/:style/:filename"

  validates :subtitle, length: { maximum: 510 }

end
