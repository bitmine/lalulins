class Sponsor < ActiveRecord::Base
  has_many :posts

  validates_presence_of :name, :address, :phone

  has_attached_file :logo,
                    :styles => {original: "100x100#"},
                    :default_url => "sem-foto2.png",
                    :url => "/images/sponsors/:attachment/:id_partition/:style/:filename",
                    :path => ":rails_root/public/images/sponsors/:attachment/:id_partition/:style/:filename"
end
