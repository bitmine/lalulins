class ContactForm < MailForm::Base
  attribute :name, :validate => true
  attribute :email, :validate => /\A([\w\.%\+\-]+)@([\w\-]+\.)+([\w]{2,})\z/i
  attribute :phone
  #attribute :file, :attachment => true

  attribute :message, :validate => true
  #attribute :nickname,  :captcha  => true

  # Declare the e-mail headers. It accepts anything the mail method
  # in ActionMailer accepts.
  def headers
    {
        :subject => "Site Lalu Lins - Contato",
        :to => "contato@lalulins.com.br",
        :from => %("#{name}" <#{email}>)
    }
  end
end
