class Post < ActiveRecord::Base
  require 'RMagick'
  require 'string_portuguese'
  include Magick
  include PgSearch

  #default_scope { order('publish_date desc') }
  has_many :photos, :dependent => :destroy
  belongs_to :category
  belongs_to :sponsor
  acts_as_ordered_taggable

  accepts_nested_attributes_for :photos, allow_destroy: true

  has_attached_file :main_image,
                    :styles => {original: "1200x3750>", big: "700x2187.5>", medium: "400x1250>"},
                    :convert_options => {original: "-quality 85", big: "-quality 90", medium: "-quality 75"},
                    :default_url => "sem-foto.png",
                    :url => "/images/posts/:attachment/:id_partition/:style/:filename",
                    :path => ":rails_root/public/images/posts/:attachment/:id_partition/:style/:filename"

  validates_presence_of :title, :content, :category_id, :publish_date, :status
  validates :main_image_subtitle, :photo_credits, :clothes_credits, length: { maximum: 255 }

  pg_search_scope :search, against: [:title, :content],
                  using: {tsearch: {dictionary: "portuguese"}},
                  associated_against: {category: :name, tags: :name},
                  ignoring: :accents

  before_save :set_soundcloud_trackid
  before_save :set_youtube_videoid
  before_save :set_spotify_uri

  def self.text_search(query)
    if query.present?
      search(query)
    else
      all
    end
  end

  def cropped_main_image(size)
    cropped_main_images_path = '/images/posts/cropped_main_images/'

    file_png = cropped_main_images_path + self.id.to_s + '_' + size.to_s + '.png'
    file_jpg = cropped_main_images_path + self.id.to_s + '_' + size.to_s + '.jpg'
    if File.exist?(Rails.root.to_s + '/public' + file_jpg)
      return file_jpg
    else
      return file_png
    end
  end

  def save_cropped_images(x, y, w, h)
    cropped_main_images_path = "#{Rails.root}/public/images/posts/cropped_main_images/"
    Dir.mkdir cropped_main_images_path unless File.directory?(cropped_main_images_path)

    main_image = ImageList.new(self.main_image.path(:original))

    cropped_image = main_image.crop(x, y, w, h)

    cropped_hotpost = cropped_image.resize_to_fit(452, 301)
    cropped_hotpost.write(cropped_main_images_path + "/" + self.id.to_s + '_hotpost.jpg')

    cropped_thumb = cropped_image.resize_to_fit(150, 100)
    cropped_thumb.write(cropped_main_images_path + "/" + self.id.to_s + '_thumb.jpg')

    img_hotpost = cropped_main_images_path + "/" + self.id.to_s + '_hotpost.jpg'
    img_facebook = cropped_main_images_path + "/" + self.id.to_s + '_facebook.jpg'
    system <<-COMMAND
    convert #{img_hotpost} -resize '300x300^' -gravity center -crop '300x300+0+0' #{img_facebook}
    COMMAND
  end

  def delete_cropped_images
    cropped_main_images_path = "#{Rails.root}/public/images/posts/cropped_main_images/"
    file_hotpost = cropped_main_images_path + self.id.to_s + '_hotpost.jpg'
    file_thumb = cropped_main_images_path + self.id.to_s + '_thumb.jpg'
    if File.exists? file_hotpost
      File.delete(cropped_main_images_path + self.id.to_s + '_hotpost.jpg')
    end
    if File.exists? file_thumb
      File.delete(cropped_main_images_path + self.id.to_s + '_thumb.jpg')
    end

    self.main_image_crop_x1 = nil
    self.main_image_crop_y1 = nil
    self.main_image_crop_x2 = nil
    self.main_image_crop_y2 = nil
  end

  def next(category_id=nil)
    post = Post.unscoped.where("publish_date >= ? and status = ? and id != ?", self.publish_date, 'Publicado', self.id)
    if category_id
      post = post.where("category_id = ?", category_id)
    end
    post.order("publish_date asc").first
  end

  def prev(category_id=nil)
    post = Post.unscoped.where("publish_date <= ? and status = ? and id != ?", self.publish_date, 'Publicado', self.id)
    if category_id
      post = post.where("category_id = ?", category_id)
    end
    post.order("publish_date desc").first
  end

  def comments_count
    post_url = "http://lalulins.com.br/post/" + self.id.to_s
    facebook_url = "http://graph.facebook.com/?ids=" + post_url
    graph_object = JSON.parse(open(facebook_url).read)
    return graph_object[post_url]['comments']
  end


  private

  def name_to_short_name(name)
    name.remover_acentos.downcase.gsub(' ', '-').tr('^A-Za-z0-9\-', '')
  end

  def set_soundcloud_trackid
    unless self.soundcloud.blank?
      match = /tracks\/(\d+)(&amp;|\&)/.match(self.soundcloud)
      unless match.nil?
        self.soundcloud_trackid = match[1].to_s
      end
    end
  end

  def set_youtube_videoid
    unless self.youtube_url.blank?
      match = /(youtu.be\/|v=)([\w|-]+)(&amp;*|\&*)(.*)/.match(self.youtube_url)
      unless match.nil?
        self.youtube_videoid = match[2].to_s
      end
    end
  end

  def set_spotify_uri
    unless self.spotify_embed.blank?
      match = /(uri=)([\w|:]+)/.match(self.spotify_embed)
      unless match.nil?
        self.spotify_uri = match[2].to_s
      end
    end
  end

end
