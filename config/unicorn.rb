working_directory "/home/garimpo/www/lalulins.com.br/"
pid "/home/garimpo/www/lalulins.com.br/tmp/pids/unicorn.pid"
stderr_path "/home/garimpo/www/lalulins.com.br/unicorn/unicorn.log"
stdout_path "/home/garimpo/www/lalulins.com.br/unicorn/unicorn.log"

listen "/tmp/lalulins-com-br.sock"
worker_processes 4
timeout 30
