/* menu-sort.js */

function update(){
    $('div.inline-related tbody tr td[class='+pos_field+']').each(function(index) {
        input = $(this).children('input');

        have = false;
        $(this).parent().find('td input[type!=hidden]').each(function(index) {
            if($(this).attr('name')!=input.attr('name') && $(this).val())
                have = true;
        });
        if(have)
            input.val(index);
        else
            input.val('');

        input.hide();
        if($(this).children('strong').html() != null)
            $(this).children('strong').html(input.attr('value'));
        else
            $(this).append($('<strong>' + input.attr('value') + '</strong>'));
        
        // Update row classes
        $(this).parent('tr').removeClass('row1');
        $(this).parent('tr').removeClass('row2');
        $(this).parent('tr').addClass('row'+((index%2)+1));
    });
}
$(document).ready(function() {
    
    pos_field = 'ordering'
    
    update();
    
    $('div.inline-related tbody').sortable({
        axis: 'y',
        items: 'tr',
        cursor: 'move',
        update: function(event, ui) {
            update();
        }
    });
    $('div.inline-related tr').css('cursor', 'move');


    $('div.inline-related tbody tr td input[type!=hidden]').change(function(){
        update();
    })
    
});