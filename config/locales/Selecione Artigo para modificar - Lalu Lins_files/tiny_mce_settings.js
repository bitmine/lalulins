function CustomFileBrowser(field_name, url, type, win) {

    var cmsURL = "/admin/filebrowser/browse/?pop=2";
    cmsURL = cmsURL + "&type=" + type;

    tinyMCE.activeEditor.windowManager.open({
        file: cmsURL,
        width: 820,  // Your dimensions may differ - toy around with them!
        height: 500,
        resizable: "yes",
        scrollbars: "yes",
        inline: "no",  // This parameter only has an effect if you use the inlinepopups plugin!
        close_previous: "no"
    }, {
        window: win,
        input: field_name,
        editor_id: tinyMCE.selectedInstance.editorId
    });
    return false;
}

tinyMCE.init({
    mode : "textareas",
    theme : "advanced",
    plugins : "table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,searchreplace,print,contextmenu,fullscreen,media,paste",

    theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontsizeselect,|,bullist,numlist,outdent,indent,|,undo,redo,|,link,unlink,anchor,|,cleanup,cut,copy,paste,|,image,media,|,code",
    theme_advanced_buttons2 : "",
    theme_advanced_buttons3 : "",
    theme_advanced_buttons4 : "",
    theme_advanced_toolbar_location : "top",
    theme_advanced_toolbar_align : "left",
    theme_advanced_statusbar_location : "bottom",
    theme_advanced_resizing : true,
    auto_cleanup_word : false,
    convert_fonts_to_spans : true,
    force_p_newlines : true,
    forced_root_block : 'p',
    
    plugin_insertdate_dateFormat : "%m/%d/%Y",
    plugin_insertdate_timeFormat : "%H:%M:%S",
    extended_valid_elements : "a[name|href|target=_blank|title|onclick],img[class|src|border=0|alt|title|hspace|vspace|width=100%|align|onmouseover|onmouseout|name|style],hr[class|width|size|noshade],font[],span[*],p[*]",
    valid_styles : { '*' : 'color,font-size,font-weight,font-style,text-decoration,text-align' },
    formats : {
        'alignleft' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', attributes: {"align":  'left'}},
        'aligncenter' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', attributes: {"align":  'center'}},
        'alignright' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', attributes: {"align":  'right'}},
        'alignfull' : {'selector' : 'p,h1,h2,h3,h4,h5,h6,td,th,div,ul,ol,li,table,img', attributes: {"align":  'justify'}}
    },
    file_browser_callback: "CustomFileBrowser",
    theme_advanced_font_sizes : "8px=8px, 10px=10px, 12px=12px, 14px=14px, 16px=16px, 18px=18px, 20px=20px, 22px=22px, 24px=24px, 26px=26px",
    // fix empty alt attributes
    verify_html : false,
    entity_encoding: 'utf-8',
    // URL
    remove_script_host : false,
    schema: "html5",
    forced_root_block : '',
    elements : 'nourlconvert',
    convert_urls : false,
    height:800,
});