Lalulins::Application.routes.draw do

  #Devise
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    passwords: 'users/passwords'
  }

  get 'tag/:tag', to: 'posts#tags', as: :tag
  get 'post/:id/', to: 'posts#post', as: :post
  get 'post/:id/:category_id', to: 'posts#post'
  get 'preview/:id/', to: 'posts#preview', as: :preview
  get 'busca/', to: 'posts#busca', as: :busca
  get 'categoria/:category_id', to: 'posts#categoria', as: :categoria
  get 'contato', to: 'home#contato', as: :contato
  post 'contato/', to: 'home#send_contato', as: :contact_forms
  get 'anuncie/', to: 'home#anuncie', as: :anuncie
  post 'anuncie/', to: 'home#send_anuncie', as: :advertise_forms
  get 'sobre', to: 'home#sobre', as: :sobre
  get 'home/index'
  get 'feed/', to: 'home#feed'

  #get ':categoria_short_name/:post_short_name', to: 'posts#get_post'

  namespace :admin do
    get 'home/index'
    resources :categories
    resources :posts
    resources :sponsors
  end

  #CKEditor
  mount Ckeditor::Engine => '/ckeditor'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'home#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase
  get 'admin' => 'admin/home#index', as: :user_root

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

end
