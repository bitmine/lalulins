#!/bin/sh
set -e
# Example init script, this can be used with nginx, too,
# since nginx and puma accept the same signals

# Feel free to change any of the following variables for your app:
TIMEOUT=${TIMEOUT-60}
APP_ROOT=/home/garimpo/www/lalulins.com.br
PID=$APP_ROOT/tmp/pids/puma.pid
SOCKET=unix:///tmp/lalulins-com-br.sock
SET_PATH="cd $APP_ROOT"
CMD="$SET_PATH; $APP_ROOT/bin/puma -C $APP_ROOT/config/puma.rb"
action="$1"
set -u

cd $APP_ROOT || exit 1

case $action in
start)
	su -c "$CMD" - garimpo
	;;
stop)
        su -c "kill -s SIGTERM `cat $PID`" - garimpo
	;;
restart|reload)
        su -c "kill -s SIGUSR2 `cat $PID`" - garimpo
	;;
*)
	echo >&2 "Usage: $0 <start|stop|restart|reload>"
	exit 1
	;;
esac

