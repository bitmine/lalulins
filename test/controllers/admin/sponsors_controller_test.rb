require 'test_helper'

class Admin::SponsorsControllerTest < ActionController::TestCase
  setup do
    @admin_sponsor = admin_sponsors(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:admin_sponsors)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create admin_sponsor" do
    assert_difference('Admin::Sponsor.count') do
      post :create, admin_sponsor: { address: @admin_sponsor.address, email: @admin_sponsor.email, facebook: @admin_sponsor.facebook, instagram: @admin_sponsor.instagram, name: @admin_sponsor.name, phone: @admin_sponsor.phone }
    end

    assert_redirected_to admin_sponsor_path(assigns(:admin_sponsor))
  end

  test "should show admin_sponsor" do
    get :show, id: @admin_sponsor
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @admin_sponsor
    assert_response :success
  end

  test "should update admin_sponsor" do
    patch :update, id: @admin_sponsor, admin_sponsor: { address: @admin_sponsor.address, email: @admin_sponsor.email, facebook: @admin_sponsor.facebook, instagram: @admin_sponsor.instagram, name: @admin_sponsor.name, phone: @admin_sponsor.phone }
    assert_redirected_to admin_sponsor_path(assigns(:admin_sponsor))
  end

  test "should destroy admin_sponsor" do
    assert_difference('Admin::Sponsor.count', -1) do
      delete :destroy, id: @admin_sponsor
    end

    assert_redirected_to admin_sponsors_path
  end
end
