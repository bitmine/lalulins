require 'test_helper'

class PostsControllerTest < ActionController::TestCase
  test "should get post" do
    get :post
    assert_response :success
  end

  test "should get busca" do
    get :busca
    assert_response :success
  end

  test "should get tags" do
    get :tags
    assert_response :success
  end

end
