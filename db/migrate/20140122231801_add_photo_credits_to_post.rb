class AddPhotoCreditsToPost < ActiveRecord::Migration
  def change
    add_column :posts, :photo_credits, :string
  end
end
