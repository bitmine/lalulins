class ChangeSubtitleTypeInPhotos < ActiveRecord::Migration
  def change
  	change_column :photos, :subtitle, :text
  end
end
