class AddYoutubeToPost < ActiveRecord::Migration
  def change
    add_column :posts, :youtube_url, :string
    add_column :posts, :youtube_videoid, :string
  end
end
