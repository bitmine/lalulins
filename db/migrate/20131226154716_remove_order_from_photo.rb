class RemoveOrderFromPhoto < ActiveRecord::Migration
  def change
    remove_column :photos, :order, :integer
  end
end
