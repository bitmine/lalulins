class AddAcknowledgmentToPost < ActiveRecord::Migration
  def change
    add_column :posts, :acknowledgment, :text
  end
end
