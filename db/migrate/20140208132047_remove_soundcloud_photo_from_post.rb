class RemoveSoundcloudPhotoFromPost < ActiveRecord::Migration
  def change
    remove_column :posts, :soundcloud_photo, :boolean
  end
end
