class AddSpotifyToPost < ActiveRecord::Migration
  def change
    add_column :posts, :spotify_uri, :string
    add_column :posts, :spotify_embed, :text
  end
end
