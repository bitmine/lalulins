class AddSponsorIdToPost < ActiveRecord::Migration
  def change
    add_column :posts, :sponsor_id, :integer
  end
end
