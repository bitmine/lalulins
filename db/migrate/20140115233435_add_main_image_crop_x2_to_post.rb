class AddMainImageCropX2ToPost < ActiveRecord::Migration
  def change
    add_column :posts, :main_image_crop_x2, :integer
  end
end
