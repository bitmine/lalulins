class AddSoundcloudShowArtworkToPost < ActiveRecord::Migration
  def change
    add_column :posts, :soundcloud_show_artwork, :boolean
  end
end
