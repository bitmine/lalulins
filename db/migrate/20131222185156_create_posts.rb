class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :title
      t.boolean :highlight
      t.text :content
      t.datetime :publish_date
      t.string :status
      t.boolean :enable_comments

      t.timestamps
    end
  end
end
