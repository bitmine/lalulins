class AddSoundcloudPhotoToPost < ActiveRecord::Migration
  def change
    add_column :posts, :soundcloud_photo, :boolean
  end
end
