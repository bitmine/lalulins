class AddSiteToSponsor < ActiveRecord::Migration
  def change
    add_column :sponsors, :site, :string
  end
end
