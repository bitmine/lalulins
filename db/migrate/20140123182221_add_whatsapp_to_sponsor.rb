class AddWhatsappToSponsor < ActiveRecord::Migration
  def change
    add_column :sponsors, :whatsapp, :string
  end
end
