class AddAttachmentMainImageToPosts < ActiveRecord::Migration
  def self.up
    change_table :posts do |t|
      t.attachment :main_image
    end
  end

  def self.down
    drop_attached_file :posts, :main_image
  end
end
