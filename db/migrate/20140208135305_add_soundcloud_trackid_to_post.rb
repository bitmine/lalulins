class AddSoundcloudTrackidToPost < ActiveRecord::Migration
  def change
    add_column :posts, :soundcloud_trackid, :integer
  end
end
