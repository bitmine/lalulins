class AddMainImageSubtitleToPost < ActiveRecord::Migration
  def change
    add_column :posts, :main_image_subtitle, :string
  end
end
