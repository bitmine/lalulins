class AddSoundcloudToPost < ActiveRecord::Migration
  def up
    add_column :posts, :soundcloud, :text
  end

  def down
    remove_column :posts, :soundcloud
  end
end
