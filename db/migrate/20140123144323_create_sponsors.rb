class CreateSponsors < ActiveRecord::Migration
  def change
    create_table :sponsors do |t|
      t.string :name
      t.string :address
      t.string :phone
      t.string :email
      t.string :instagram
      t.string :facebook

      t.timestamps
    end
  end
end
