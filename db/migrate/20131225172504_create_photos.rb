class CreatePhotos < ActiveRecord::Migration
  def change
    create_table :photos do |t|
      t.string :subtitle
      t.string :credits
      t.integer :order

      t.timestamps
    end
  end
end
